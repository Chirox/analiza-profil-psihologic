﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{

    public Image image;
    public Text stepText;


    [Header("Configuration Panel")]
    public float binarizationThreshold; // recommended is 0.5f
    public Image imagePrefab;
    public Color backgroundColor;
    public Texture2D copyTexture;
    public Text finalResult;
    public Button analyzeButton;

    int currentStep;
    List<Element> elements = new List<Element>();

    List<Color> colorsToCheck = new List<Color> { Color.red, Color.green, Color.black, Color.blue, Color.magenta};
    List<string> messages = new List<string>{    "Culoare predominanta este rosu. Acest lucru denota o furie intensa.",
                                                 "Culoarea predominanta este verde. Aceasta culoare denota liniste si calm",
                                                 "Culoarea predominanta este negru. Acest lucru indica o posibila stare depresiva.",
                                                 "Culoarea predominanta este albastru. Aceasta culoare denota liniste si calm.",
                                                 "Culoarea predominanta este mov. Acest lucru denota lipsa de atentie din partea persoanelor apropiate."
    };
                                                 
    List<float> scores = new List<float> { 0.5f, 0.1f, 0.5f, 0.1f, 0.15f };

    //};
    /// <summary>
    /// Class used to store data for each element in the original picture
    /// </summary>
    public class Element
    {
         // used to determine the coresponding area in the original picture
        public Vector2Int lowerLeft, upperRight;

        public List<Vector2Int> pixelList = new List<Vector2Int>();
        
        public Color ComputeMediumColor()
        {
            return Color.black;
        }

        public int ComputeArea()
        {
            Vector2Int sizeDelta = GetSizeDelta();
            return sizeDelta.x * sizeDelta.y;
        }     

        public Vector2Int GetSizeDelta()
        {
            
            return new Vector2Int(upperRight.x - lowerLeft.x + 1, upperRight.y - lowerLeft.y + 1);
        }
    }


    public void ApplyGrayscaleCCIR601(Texture2D texture)
    {    
        float a = 0.2989f, b = 0.587f, c = 0.072f;
        float width = texture.width;
        float heigth = texture.height;

        for(int i=0; i<width; i++)
        {
            for(int j=0; j<heigth; j++)
            {
                Color color = texture.GetPixel(i, j);
                float y = a * color.r + b * color.g + c * color.b;
                texture.SetPixel(i, j, new Color(y, y, y));
            }
        }
        texture.Apply();     
    }

    public void Binarize(Texture2D texture)
    {
        float width = texture.width;
        float heigth = texture.height;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < heigth; j++)
            {
                Color color = texture.GetPixel(i, j);

                if (color.r < binarizationThreshold)
                {
                    texture.SetPixel(i, j, Color.black);
                }
                else
                {
                    texture.SetPixel(i, j, Color.white);
                }
            }
        }
        texture.Apply();
    }

    public List<Element> ApplyBFS(Texture2D texture)
    {
        List<Element> elementList = new List<Element>();

        int[] dirX = { 1, 0, -1, 0 };
        int[] dirY = { 0, 1, 0, -1 };

        int width = texture.width;
        int heigth = texture.height;

        bool[,] visited = new bool[texture.width, texture.height];

        for(int i=0; i<width; i++)
        {
            for(int j=0; j<heigth; j++)
            {
                if(visited[i,j] == false && texture.GetPixel(i, j).r < 0.5f)
                {
                    Element element = new Element();
                    element.upperRight = new Vector2Int(i, j);
                    element.lowerLeft = new Vector2Int(i,j);
                    
                    Queue<Vector2Int> toVisit = new Queue<Vector2Int>();
                    toVisit.Enqueue(new Vector2Int(i, j)); visited[i, j] = true;
                    element.pixelList.Add(new Vector2Int(i, j));
                    do
                    {
                        Vector2Int coord = toVisit.Dequeue();
                        for (int k = 0; k < 4; k++)
                        {
                            Vector2Int newCoord = coord + new Vector2Int(dirX[k], dirY[k]);
                            if (newCoord.x < 0) break;
                            if (newCoord.x >= width) break;
                            if (newCoord.y < 0) break;
                            if (newCoord.y >= heigth) break;

                            if (texture.GetPixel(newCoord.x, newCoord.y).r < 0.5f &&  visited[newCoord.x, newCoord.y] == false)
                            {
                                toVisit.Enqueue(newCoord); visited[newCoord.x, newCoord.y] = true; //texture.SetPixel(newCoord.x, newCoord.y, Color.red);
                                element.pixelList.Add(new Vector2Int(newCoord.x, newCoord.y));

                                if (newCoord.x < element.lowerLeft.x) element.lowerLeft.x = newCoord.x;
                                if (newCoord.y < element.lowerLeft.y) element.lowerLeft.y = newCoord.y;

                                if (newCoord.x > element.upperRight.x) element.upperRight.x = newCoord.x;
                                if (newCoord.y > element.upperRight.y) element.upperRight.y = newCoord.y;
                            }

                        }
                    }
           
                    while (toVisit.Count > 0);

                    elementList.Add(element);
                }
            }
        }
        texture.Apply();
        return elementList;
    }


    public void Analyze()
    {
        currentStep++;       

        switch (currentStep)
        {
            case 1:
                copyTexture = new Texture2D(image.sprite.texture.width, image.sprite.texture.height);
                copyTexture.SetPixels(image.sprite.texture.GetPixels());
                copyTexture.Apply();
                ApplyGrayscaleCCIR601(image.sprite.texture);
                stepText.text = "Continua";                
                break;
            case 2:
                Binarize(image.sprite.texture);
                break;
            case 3:
                elements = ApplyBFS(image.sprite.texture);
                image.color = new Color(0, 0, 0, 0);
                Debug.Log(elements.Count);
                foreach (Element element in elements)
                {
                    if (element.ComputeArea() > 100)
                    {
                        Image imageElement = Instantiate(imagePrefab);
                        
                        imageElement.transform.SetParent(image.rectTransform);
                        
                        
                        Vector2Int sizeDelta = element.GetSizeDelta();

                        imageElement.rectTransform.sizeDelta = new Vector2(sizeDelta.x, sizeDelta.y);
                        imageElement.rectTransform.localScale = Vector2.one;

                        imageElement.rectTransform.localPosition = new Vector2(element.lowerLeft.x, element.lowerLeft.y);
                        imageElement.rectTransform.anchoredPosition = Vector2.zero;

                        imageElement.rectTransform.anchoredPosition = new Vector2(element.lowerLeft.x, element.lowerLeft.y);

                        Texture2D texture = new Texture2D(sizeDelta.x, sizeDelta.y);
                        

                        for(int i=0; i<sizeDelta.x; i++)
                        {
                            for(int j=0; j<sizeDelta.y; j++)
                            {
                                texture.SetPixel(i, j, backgroundColor);
                            }
                        }
                        foreach(Vector2Int coord in element.pixelList)
                        {
                            texture.SetPixel(coord.x - element.lowerLeft.x, coord.y - element.lowerLeft.y, copyTexture.GetPixel(coord.x, coord.y));
                        }

                        texture.Apply();
                        imageElement.sprite = Sprite.Create(texture, new Rect(0, 0, sizeDelta.x, sizeDelta.y), new Vector2(0.5f, 0.5f));
                    }
                }
                break;
            case 4:
                analyzeButton.gameObject.SetActive(false);
                AnalyzeElementsAndOutputResult(image.rectTransform);
                break;
            default:
                break;
        }

        Debug.Log("Done Etapa " + currentStep);
    }

    public void AnalyzeElementsAndOutputResult(RectTransform elementsParent)
    {
        float mediumXPosition = 0;

        Color mediumColor = new Color(0,0,0);

        int actualPixels = 0;

        int[] apar = new int[5];

        int index = 0;
        int maxIndex = 0;


        for (int k=0; k<elementsParent.childCount; k++)
        {
            Texture2D texture = elementsParent.GetChild(k).GetComponent<Image>().sprite.texture;
            mediumXPosition += elementsParent.GetChild(k).GetComponent<RectTransform>().localPosition.x + 320 + texture.width/2;

            for (int i=0; i< texture.width; i++)
            {
                for(int j=0; j<texture.height; j++)
                {
                    if(texture.GetPixel(i, j).a > 0.5f)
                    {
                        mediumColor += texture.GetPixel(i, j);
                        actualPixels++;

                        float distMin = float.MaxValue;
                       
                        for(int c=0; c<colorsToCheck.Count; c++)
                        {
                            if(EuclidianDistance(colorsToCheck[c], texture.GetPixel(i, j) ) < distMin)
                            {
                                index = c;
                                distMin = EuclidianDistance(colorsToCheck[c], texture.GetPixel(i, j));
                            }
                        }
                    }
                    apar[index]++;
                    
                }
            }
        }
        float maxValue = -1;
        for(int i=0; i<5; i++)
        {
            Debug.Log(apar[i]);
            if(apar[i] > maxValue)
            {
                maxIndex = i;
                maxValue = apar[i];
            }
        }

        float totalScore;

        mediumXPosition = (mediumXPosition / elementsParent.childCount) / 640.0f;

        finalResult.text = "REZULTAT ANALIZA\n";
        finalResult.text += "Pozitionare medie elemente: " + ((int)(mediumXPosition * 100)).ToString() + "% fata de marginea stanga. ";

        System.Tuple<string, float> result = AnalysePosition(mediumXPosition);
        finalResult.text += result.Item1 + " ";
        totalScore = result.Item2;

        Debug.Log(mediumColor);
        mediumColor = new Color(mediumColor.r / actualPixels, mediumColor.g / actualPixels, mediumColor.b / actualPixels);
        Debug.Log(mediumColor);
    
        finalResult.text += messages[maxIndex];
        totalScore += scores[maxIndex];

       

      //  finalResult.text += GetTheMostSimilarColorMessage(mediumColor);

        if (totalScore < 0.5f)
        {
            finalResult.text += " Nu sunt necesare investigatii suplimentare.";
        }
        else
        {
            finalResult.text += " Sunt necesare investigatii suplimentare.";
        }
    }


    public System.Tuple<string, float> GetTheMostSimilarColorMessage(Color mediumColor)
    {
        string message = "";
        float score = 0;
        float currentDistance = float.MaxValue;

        for(int i=0; i<colorsToCheck.Count; i++)
        {
            float distance = EuclidianDistance(mediumColor, colorsToCheck[i]);
            Debug.Log(mediumColor);
            if (distance < currentDistance)
            {
                message = messages[i];
                score = scores[i];
                currentDistance = distance;
            }
        }
        return new System.Tuple<string, float>(message, score);
    }

    public float EuclidianDistance(Color color1, Color color2)
    {
        return Mathf.Sqrt(Mathf.Pow(color1.r - color2.r, 2) + Mathf.Pow(color1.g - color2.g, 2) + Mathf.Pow(color1.b - color2.b, 2));
    }

    public System.Tuple<string, float> AnalysePosition(float position)
    {
        if(position < 0.35f)
        {
            return new System.Tuple<string, float> ("Pozitionarea cu precadere in stanga este un semn de introversiune.", 0.35f);
        }
        if(position > 0.65f)
        {
            return new System.Tuple<string, float>("Pozitionarea cu precadere in dreapta este un semn de extroversiune.", 0.35f);
        }
        return new System.Tuple<string, float>( "Pozitionarea este una echilibrata.", 0.1f);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}