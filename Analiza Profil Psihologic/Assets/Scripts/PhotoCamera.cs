﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
public class PhotoCamera : MonoBehaviour
{

    WebCamTexture webcamTexture;
    public int requestedWidth = 640;
    public int requestedHeight = 480;
    float captureIntervalSeconds = 0.1f;

    //Used by the camera
    public RawImage rawImage;

    //Used to store the snapshot
    public Image image;


    public void Start()
    {
  
        WebCamDevice[] devices = WebCamTexture.devices;
        for (var i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
        }
        if (devices.Length > 0)
        {
            webcamTexture = new WebCamTexture(devices[0].name, requestedWidth, requestedHeight);
            rawImage.GetComponent<RawImage>().texture = webcamTexture;
            webcamTexture.Play();     
        }
     
    }

    private IEnumerator Capture()
    {
        yield return new WaitForSeconds(captureIntervalSeconds);

        Color[] pixels = webcamTexture.GetPixels();


        Texture2D texture2D = null;

        // webcamTexture.
        if (pixels.Length == 0)
            yield return null;
        if (texture2D == null || webcamTexture.width != texture2D.width || webcamTexture.height != texture2D.height)
        {
            texture2D = new Texture2D(webcamTexture.width, webcamTexture.height, TextureFormat.RGBA32, false);
        }

        webcamTexture.Stop();
        texture2D.SetPixels(pixels);
        texture2D.Apply();
        rawImage.enabled = false;

        image.sprite = image.sprite = Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0, 0), 100, 0, SpriteMeshType.Tight);
    }

    public void TakePhoto()
    {
        StartCoroutine("Capture");
    }
}